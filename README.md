Run these 3 commands on unix-like terminal to create 3 files with 6291556 bytes, 4718589 bytes and 4718586 bytes size.

```
dd if=/dev/zero of=6291556 bs=6291556 count=1
dd if=/dev/zero of=4718589 bs=4718589 count=1
dd if=/dev/zero of=4718586 bs=4718586 count=1
```

- GET /json  
Normal JSON response body
- GET /error  
Return response that cannot be JSON.stringified and throw in lambda  
- GET /image  
Return binary image response
- GET /largeImage  
Redirect client to other url
- POST /input  
Logging event body in Cloudwatch for testing base64 encoded request body
- GET /file  
Return 3 file (4718586, 4718589 or 6291556) for testing lambda 6mb payload limit